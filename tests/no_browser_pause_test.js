suite('Add preferred card and add items from various locations -', function() {

    setup(function() {
        testBase.signInFromBanner(TestData.smokeOrderData.stageEmail2, TestData.smokeOrderData.password);
    });

    teardown(function() {
        headerComponent.goToHome();
        headerComponent.signOut();
    });

    test('Preferred car, select time slot', function(){
        let verify = kroger_verify();

        assert.isTrue(bannerHomePage.isCurrent(), 'Test did not navigate back to home page after signing in');
        assert.isTrue(bannerHomePage.isCLHeaderPresent(), 'Header not present on CLW home page');
        verify.isTrue(bannerHomePage.isCLFooterPresent(), 'Footer must be present.');
        headerComponent.goToMyAccount();

        assert.isTrue(accountSummaryPage.isCurrent(),'Test did not navigate to Account Summary page..')
        userFullName = accountSummaryPage.fullName();
        userMobileNumber = accountSummaryPage.userMobileNumber().replace(/\D/g, '');

        accountSummaryPage.goToMyWallet();
        browser.pause(2000);
        assert.isTrue(myWallet.isCurrent(), 'Test did not navigate to My Wallet page');

    });
});
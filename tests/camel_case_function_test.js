function testingVal__a(){
    var functionVariableA = 11;
}

class PreferredStore  {
    get preferredStoreHeader()  {return $('.StoreDetails-header');}
    get pageHeaderLocator()     {return $('.StoreInformation-storeName');}
    get storeAddress()          {return $('.StoreInformation-wrapper span:nth-of-type(1) span .StoreAddress-storeAddressGuts');}
    TsCurrent() {
        if(wdioCommands.waitForPageToLoad(this.pageHeaderLocator,3000, 3, false) ){
            if(wdioCommands.waitForText(this.pageHeaderLocator) == 'Sharonville, OH Grocery Store'){
                console.log('Navigated to Preferred store Page..');
                return true;
            } else {return false;}
        }
        return false;
    }
    preferred_Store_address() {
        let Address = wdioCommands.waitForText(this.storeAddress);
        Address = Address.split('\n')[0];
        return Address;
    }
}
export default new PreferredStore();
const firstNameLocator                   = '#newPatientFirstName';
const lastNameLocator                    = '#newPatientLastName';
const monthLocator                       = '#newPatientDob > select.date-input-month';
const dayLocator                         = 'html/body/div[1]/section/div[1]/div/div/div/div[1]';
const yearLocator                        = '#newPatientDob > input.date-input-year';
const addressLocator                     = '#newPatientAddress';
const cityLocator                        = '#newPatientCity';

class AccountSummaryPage{

    get purchase()                      {return $('#MyPurchases');}
    get accountSummaryHeader()          {return $('#account-summary');}
    get accountSummaryContainer()       {return $('div.account-summary-container');}
    get displayedEmailAddressLoc()      {return $('.AccountCredentials .ConfirmableInput:nth-of-type(1) .accountManagement-subSectionValue');}
    get displayedPreferredStoreLoc()    {return $('//*[contains(@name,"btn")]');}
    get displayedAltIDLoc()             {return $('#alt-id');}
    get displayedNameLoc()              {return $('#nameValue');}
    get preferredStoreEdit()            {return $('#preferred-store-view-edit-button');}
    get preferredStoreEditZipcode()     {return $('//*[@type="text"]//following::input[1]');}


}


const HomePage = Object.create(this, {
    isCurrent: {
        value: function () {
            console.log('Checking that the Home Page is the current page.');
            browser.waitForVisible(pageHeaderLocator, 120000);
            return browser.getText(pageHeaderLocator) == 'Home';
        }
    },
    findOrderUsingOrderDetails: {
        value: function (orderNum) {
            console.log('Finding Order with valid inputs that navigates to Order Summary page.');
            browser.waitForEnabled(orderNumberTextBoxLocator);
            browser.setValue(orderNumberTextBoxLocator, orderNum);
            browser.waitForEnabled(findOrderInOrderLocator);
            return browser.getText(pageHeaderLocator) != 'Home';
        }
    }
});
module.exports = HomePage;